import {Axios} from '../utils/AxiosPlugin'

const getPnLogOperateByPage = async function (params = {}) {
  return await Axios.post('/logOperate/getPnLogOperateByPage', params);
};

const uncompressLogOperateLayoutStr = async function (log_id) {
  return await Axios.post('/logOperate/uncompressLogOperateLayoutStr', {id: log_id});
};

const clearLogOperate = async function () {
  return await Axios.post('/logOperate/clearLogOperate');
};

const deleteLogOperateById = async function (id) {
  return await Axios.delete('/logOperate/deleteLogOperateById', {params: {id: id}});
}

export default {
  getPnLogOperateByPage,
  uncompressLogOperateLayoutStr,
  clearLogOperate,
  deleteLogOperateById
}
