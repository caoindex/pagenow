import {Axios} from '../utils/AxiosPlugin'

const getAttachmentInfoByPage = async function (pageIndex, pageSize, fileName = '', dirPath = '') {
  return await Axios.post('/attachment/getAttachmentInfoByPage', {pageIndex: pageIndex, pageSize: pageSize, fileName: fileName, dirPath: dirPath});
};

const deleteNotUseDir = async function () {
  return await Axios.post('/attachment/deleteNotUseDir');
};

const clearTempDir = async function () {
  return await Axios.post('/attachment/clearTempDir');
};

export default {
  getAttachmentInfoByPage,
  deleteNotUseDir,
  clearTempDir
}
