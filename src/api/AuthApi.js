import {Axios} from '../utils/AxiosPlugin'

const login = async function (loginForm) {
  return await Axios.post('/auth/login', loginForm);
};

const logout = async function (token) {
  return await Axios.post('/auth/logout', {token: token})
};

export default {
  login,
  logout
}
