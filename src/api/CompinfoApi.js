import {Axios} from '../utils/AxiosPlugin'

// -----------------------分组管理-------------------------

const saveCompinfoType = async function (compInfoType) {
  return await Axios.post('/compInfo/saveCompInfoType', compInfoType);
};

const deleteCompinfoType = async function (id) {
  return await Axios.delete('/compInfo/deleteCompInfoType', {params: {id: id}});
};

const getCompinfoTypeByPage = async function (pageIndex, pageSize, name) {
  return await Axios.post('/compInfo/getCompInfoTypeByPage', {pageIndex: pageIndex, pageSize: pageSize, name: name});
};

const getAllCompinfoType = async function () {
  return await Axios.get('/compInfo/getAllCompInfoType');
};

const getCompinfoTypeById = async function (id) {
  return await Axios.get('/compInfo/getCompInfoTypeById', {params: {id: id}})
}

// -----------------------分组管理------------------------- end

const saveCompinfo = async function (compInfo) {
  return await Axios.post('/compInfo/saveCompInfo', compInfo);
};

const getCompinfoById = async function (id) {
  return await Axios.get('/compInfo/getCompInfoById', {params: {id: id}});
};

const getAllCompinfo = async function () {
  return await Axios.get('/compInfo/getAllCompInfo');
};

const getCompinfoByPage = async function (pageIndex, pageSize, type_id) {
  return await Axios.post('/compInfo/getCompInfoByPage', {pageIndex: pageIndex, pageSize: pageSize, type_id: type_id});
};

const deleteCompinfo = async function (id) {
  return await Axios.delete('/compInfo/deleteCompInfo', {params: {id: id}});
};

const getAllCompinfoByTypeId = async function (type_id) {
  return await Axios.get('/compInfo/getAllCompInfoByTypeId', {params: {type_id: type_id}});
};

const importCompinfoJson = async function (jsonData, compInfoTypeId) {
  return await Axios.post('/compInfo/importCompInfoJson', {jsonData: jsonData, compInfoTypeId: compInfoTypeId});
};

export default {
  saveCompinfoType,
  deleteCompinfoType,
  getCompinfoTypeByPage,
  getAllCompinfoType,
  getCompinfoTypeById,
  saveCompinfo,
  getCompinfoById,
  getAllCompinfo,
  getCompinfoByPage,
  deleteCompinfo,
  getAllCompinfoByTypeId,
  importCompinfoJson
}
