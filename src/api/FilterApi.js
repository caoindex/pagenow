import {Axios} from '../utils/AxiosPlugin'

const saveFilter = async function (filter) {
  return await Axios.post('/filter/saveFilter', filter);
};

const getAllFilter = async function () {
  return await Axios.get('/filter/getAllFilter');
};

const getFilterById = async function (id) {
  return await Axios.get('/filter/getFilterById', {params: {id: id}});
};

const deleteFilter = async function (id) {
  return await Axios.delete('/filter/deleteFilter', {params: {id: id}});
};

const copyFilter = async function (id) {
  return await Axios.post('/filter/copyFilter', {id: id});
};

export default {
  saveFilter,
  getAllFilter,
  getFilterById,
  deleteFilter,
  copyFilter
}
